# activity_profile_tool

This tool permits generating and editing an activity profile (sequence of operational modes) of a satellite in Earth orbit

This is only a mirror repository. Original repo is [here](https://github.com/uc3m-aerospace/activity_profile_tool).

```
git clone https://github.com/uc3m-aerospace/activity_profile_tool.git
```




